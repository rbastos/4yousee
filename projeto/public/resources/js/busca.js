function results() {

    $("#page-search").click(function(e){
		e.preventDefault();

		var json = "",
		request = new XMLHttpRequest(),
		listResults = "",
		tipo = $("#tipo").val(),
		categoria = $("#categoria").val(),
		flag = false;

		if((tipo =="" || tipo == undefined) &&(categoria =="" || tipo == categoria)){
			$("#tipo").addClass("border-danger");
			$("#categoria").addClass("border-danger");
			$("#modalError").modal('show');

			$("#categoria, #tipo").change(function(){
				$("#tipo").removeClass("border-danger");
				$("#categoria").removeClass("border-danger");

			});
		}


		request.open('GET', 'https://private-anon-6480910073-4youseesocialtest.apiary-mock.com/timeline');

		request.onreadystatechange = function () {
		  if (this.readyState === 4) {
		  	
			var cont = 0;
			
			json = JSON.parse(this.responseText);


			for (var i = 0; i < json.length; i++) {
				var my = "";
				if(flag){
					my = 'my-4';
				}

				if(json[i].type == tipo){
					listResults += 
					'<li class="media '+my+'">'+
				    	'<a href="pages/'+json[i].type+'.html">'+
				    		'<img class="mr-3 img-thumbnail thumb-hold" src="'+json[i].thumbnail+'">'+
				    	'</a>'+
				    	'<div class="media-body">'+
				    		'<a href="#">'+
					      		'<h5 class="mt-0 mb-1">'+json[i].title+'</h5>'+
					      	'</a>'+
					      	json[i].description+
				    	'</div>'+
				  	'</li>';

				  	if (flag) {
				  		flag = false;
				  	} else {
				  		flag = true;
				  	}
				}

				cont++;
			}

			cont = listResults.length + 1;
			for (var i = 0; i < json.length; i++) {

				if(json[i].category == categoria){
					listResults += 
					'<li class="media">'+
				    	'<a href="pages/'+json[i].type+'.html">'+
				    		'<img class="mr-3 img-thumbnail thumb-hold" src="'+json[i].thumbnail+'">'+
				    	'</a>'+
				    	'<div class="media-body">'+
				    		'<a href="#">'+
					      		'<h5 class="mt-0 mb-1">'+json[i].title+'</h5>'+
					      	'</a>'+
					      	json[i].description+
				    	'</div>'+
				  	'</li>'
				}

				cont++;
			}

			$("#results").html(listResults);
			
		  }
		};

		request.send();

	})
}